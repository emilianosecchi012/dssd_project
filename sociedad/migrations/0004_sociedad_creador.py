# Generated by Django 3.1.12 on 2021-10-18 20:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sociedad', '0003_auto_20211007_1716'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociedad',
            name='creador',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='sociedad_duenio', to=settings.AUTH_USER_MODEL),
        ),
    ]
