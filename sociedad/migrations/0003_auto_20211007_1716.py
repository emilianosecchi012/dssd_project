# Generated by Django 3.1.12 on 2021-10-07 17:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sociedad', '0002_auto_20211007_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paisestado',
            name='pais_codigo',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
