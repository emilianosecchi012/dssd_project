from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import DetailView, FormView, ListView

from django.forms.models import model_to_dict
from datetime import date, timedelta
import time

from core.views import ConfirmView
from users.mixins import EsEmpleadoMesaEntradaMixin, EsEmpleadoAreaLegalesMixin, EsApoderadoMixin
from users.models import User
from .helpers import crear_sociedad, editar_sociedad, revisar_sociedad, rechazar_sociedad, confirmar_sociedad, confirmar_sociedad_automatica, rechazar_confirmacion_sociedad, solicitar_estampillado, generar_qr, generar_carpetas, ConectorAPIBonita
from .forms import SociedadForm, SociedadEditarForm, RechazarForm, RechazarConfirmacionForm, PaisEstadoFormSet, DomicilioRealFormSet, DomicilioLegalFormSet, SocioFormSet
from .models import Sociedad


class Registrar(LoginRequiredMixin, EsApoderadoMixin, FormView):
    template_name = 'sociedad/registrar.html'
    form_class = SociedadForm

    def get_context_data(self, **kwargs):
        context = super(Registrar, self).get_context_data(**kwargs)
        context['title'] = 'Sociedad'
        context['url_cancelar'] = reverse_lazy('sociedades:listar')
        
        if 'formset' not in context:
            context['formset'] = PaisEstadoFormSet(prefix='exporta')
        if 'form_dom_real' not in context:
            context['form_dom_real'] = DomicilioRealFormSet(prefix='dom_real')
        if 'form_dom_legal' not in context:
            context['form_dom_legal'] = DomicilioLegalFormSet(prefix='dom_legal')
        if 'form_socio' not in context:
            context['form_socio'] = SocioFormSet(prefix='socios')

        return context

    def post(self, request, *args, **kwargs):
        form = SociedadForm(self.request.POST, self.request.FILES)

        formset = PaisEstadoFormSet(self.request.POST, prefix='exporta')

        form_dom_real = DomicilioRealFormSet(self.request.POST, prefix='dom_real')

        form_dom_legal = DomicilioLegalFormSet(self.request.POST, prefix='dom_legal')

        form_socio = SocioFormSet(self.request.POST, prefix='socios')

        if form.is_valid() and formset.is_valid() and form_dom_real.is_valid() and form_dom_legal.is_valid() and form_socio.is_valid():
            return self.form_valid(form, formset, form_dom_real, form_dom_legal, form_socio)
        else:
            return self.form_invalid(form, formset, form_dom_real, form_dom_legal, form_socio)

    def form_valid(self, form, formset, form_dom_real, form_dom_legal, form_socio):
        nombre = form.cleaned_data['nombre']
        fecha_creacion = form.cleaned_data['fecha_creacion']
        archivo_estatuto = form.cleaned_data['archivo_estatuto']
        email = form.cleaned_data['email']

        domicilio_real = {
            'pais_dom_real' : form_dom_real[0].cleaned_data['pais_dom_real'],
            'estado_dom_real' : form_dom_real[0].cleaned_data['estado_dom_real'],
            'localidad' : form_dom_real[0].cleaned_data['localidad'],
            'codigo_postal' : form_dom_real[0].cleaned_data['codigo_postal'],
            'calle' : form_dom_real[0].cleaned_data['calle'],
            'numero' : form_dom_real[0].cleaned_data['numero'],
            'piso' : form_dom_real[0].cleaned_data['piso'],
            'departamento' : form_dom_real[0].cleaned_data['departamento']
        }

        domicilio_legal = {
            'pais_dom_legal' : form_dom_legal[0].cleaned_data['pais_dom_legal'],
            'estado_dom_legal' : form_dom_legal[0].cleaned_data['estado_dom_legal'],
            'localidad' : form_dom_legal[0].cleaned_data['localidad'],
            'codigo_postal' : form_dom_legal[0].cleaned_data['codigo_postal'],
            'calle' : form_dom_legal[0].cleaned_data['calle'],
            'numero' : form_dom_legal[0].cleaned_data['numero'],
            'piso' : form_dom_legal[0].cleaned_data['piso'],
            'departamento' : form_dom_legal[0].cleaned_data['departamento']
        }

        socios = []
        for f in form_socio:
            socios.append({
                'nombres' : f.cleaned_data['nombres'],
                'apellidos' : f.cleaned_data['apellidos'],
                'aporte' : f.cleaned_data['aporte'],
                'apoderado' : f.cleaned_data['apoderado']
            })

        exportaciones = []
        for f in formset:
            try:
                if f.cleaned_data['pais'] != '':
                    exportaciones.append({
                        'pais_codigo' : f.cleaned_data['pais'],
                        'estado_descripcion' : f.cleaned_data['estado']
                    })
            except KeyError:
                pass

        agregado = crear_sociedad(self.request.user, nombre, fecha_creacion, archivo_estatuto, email, domicilio_real, domicilio_legal, socios, exportaciones)

        if agregado:
            conectorAPIBonita = ConectorAPIBonita()
            agregado.id_caso = conectorAPIBonita.instanciar_caso(self.request.user)
            agregado.save()
            messages.success(self.request, 'Se cargó correctamente la sociedad')
        else:
            messages.error(self.request, 'No se pudo cargar la sociedad')
        
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, formset, form_dom_real, form_dom_legal, form_socio):
        return self.render_to_response(self.get_context_data(form=form, formset=formset, form_dom_real=form_dom_real, form_dom_legal=form_dom_legal, form_socio=form_socio))

    def get_success_url(self):
        return reverse_lazy('sociedades:listar')


class Editar(LoginRequiredMixin, EsApoderadoMixin, FormView):
    template_name = 'sociedad/registrar.html'
    form_class = SociedadEditarForm

    def get_context_data(self, **kwargs):
        context = super(Editar, self).get_context_data(**kwargs)
        context['title'] = 'Sociedad'
        context['url_cancelar'] = reverse_lazy('sociedades:sociedad', kwargs={'pk': self.kwargs['pk']})

        sociedad = Sociedad.objects.get(pk=self.kwargs['pk'])
        context['form'] = SociedadEditarForm(initial=model_to_dict(sociedad))
        context['formset'] = PaisEstadoFormSet(prefix='exporta')
        context['form_dom_real'] = DomicilioRealFormSet(prefix='dom_real')
        context['form_dom_legal'] = DomicilioLegalFormSet(prefix='dom_legal')
        context['form_socio'] = SocioFormSet(prefix='socios')

        return context

    def post(self, request, *args, **kwargs):
        form = SociedadEditarForm(self.request.POST, self.request.FILES)

        formset = PaisEstadoFormSet(self.request.POST, prefix='exporta')

        form_dom_real = DomicilioRealFormSet(self.request.POST, prefix='dom_real')

        form_dom_legal = DomicilioLegalFormSet(self.request.POST, prefix='dom_legal')

        form_socio = SocioFormSet(self.request.POST, prefix='socios')

        if form.is_valid() and formset.is_valid() and form_dom_real.is_valid() and form_dom_legal.is_valid() and form_socio.is_valid():
            return self.form_valid(form, formset, form_dom_real, form_dom_legal, form_socio)
        else:
            return self.form_invalid(form, formset, form_dom_real, form_dom_legal, form_socio)

    def form_valid(self, form, formset, form_dom_real, form_dom_legal, form_socio):
        nombre = form.cleaned_data['nombre']
        fecha_creacion = form.cleaned_data['fecha_creacion']
        email = form.cleaned_data['email']

        sociedad = Sociedad.objects.get(pk=self.kwargs['pk'])

        domicilio_real = {
            'pais_dom_real' : form_dom_real[0].cleaned_data['pais_dom_real'],
            'estado_dom_real' : form_dom_real[0].cleaned_data['estado_dom_real'],
            'localidad' : form_dom_real[0].cleaned_data['localidad'],
            'codigo_postal' : form_dom_real[0].cleaned_data['codigo_postal'],
            'calle' : form_dom_real[0].cleaned_data['calle'],
            'numero' : form_dom_real[0].cleaned_data['numero'],
            'piso' : form_dom_real[0].cleaned_data['piso'],
            'departamento' : form_dom_real[0].cleaned_data['departamento']
        }

        domicilio_legal = {
            'pais_dom_legal' : form_dom_legal[0].cleaned_data['pais_dom_legal'],
            'estado_dom_legal' : form_dom_legal[0].cleaned_data['estado_dom_legal'],
            'localidad' : form_dom_legal[0].cleaned_data['localidad'],
            'codigo_postal' : form_dom_legal[0].cleaned_data['codigo_postal'],
            'calle' : form_dom_legal[0].cleaned_data['calle'],
            'numero' : form_dom_legal[0].cleaned_data['numero'],
            'piso' : form_dom_legal[0].cleaned_data['piso'],
            'departamento' : form_dom_legal[0].cleaned_data['departamento']
        }

        socios = []
        for f in form_socio:
            socios.append({
                'nombres' : f.cleaned_data['nombres'],
                'apellidos' : f.cleaned_data['apellidos'],
                'aporte' : f.cleaned_data['aporte'],
                'apoderado' : f.cleaned_data['apoderado']
            })

        exportaciones = []
        for f in formset:
            try:
                if f.cleaned_data['pais'] != '':
                    exportaciones.append({
                        'pais_codigo' : f.cleaned_data['pais'],
                        'estado_descripcion' : f.cleaned_data['estado']
                    })
            except KeyError:
                pass

        agregado = editar_sociedad(self.request.user, sociedad, nombre, fecha_creacion, email, domicilio_real, domicilio_legal, socios, exportaciones)

        if agregado:
            ConectorAPIBonita().actualizar_variables(self.request.user, agregado.id_caso, agregado.nombre, agregado.fecha_creacion, agregado.email, agregado.domicilio_real, agregado.domicilio_legal, agregado.socio.all(), agregado.paises_exportados.all())
            messages.success(self.request, 'Se editó correctamente la sociedad')
        else:
            messages.error(self.request, 'No se pudo editar la sociedad')
        
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, formset, form_dom_real, form_dom_legal, form_socio):
        return self.render_to_response(self.get_context_data(form=form, formset=formset, form_dom_real=form_dom_real, form_dom_legal=form_dom_legal, form_socio=form_socio))

    def get_success_url(self):
        return reverse_lazy('sociedades:sociedad', kwargs={'pk': self.kwargs['pk']})


class Listar(LoginRequiredMixin, ListView):
    model = Sociedad
    template_name = 'sociedad/listar.html'
    context_object_name = 'sociedades'

    def get_queryset(self):
        if self.request.user.empleado_mesa_entrada:
            queryset = super(Listar, self).get_queryset().filter(estado=Sociedad.CERRADA) | super(Listar, self).get_queryset().filter(estado=Sociedad.CONFIRMADA)
            return queryset.order_by('-pk')
        elif self.request.user.empleado_area_legales:
            return super(Listar, self).get_queryset().filter(estado=Sociedad.REVISADA).order_by('-pk')
        else:
            return super(Listar, self).get_queryset().filter(creador=self.request.user).order_by('-pk')


class SociedadVer(LoginRequiredMixin, DetailView):
    model = Sociedad
    template_name = 'sociedad/ver.html'
    context_object_name = 'sociedad'

    def get_object(self, queryset=None):
        sociedad = Sociedad.objects.filter(pk=self.kwargs['pk'])
        if sociedad.count() > 0:
            sociedad=sociedad.first()
            if sociedad.fecha_rechazo:
                fecha_fin = sociedad.fecha_rechazo + timedelta(days=sociedad.plazo_correccion)
                if date.today() > fecha_fin:
                    sociedad.estado = Sociedad.CANCELADA
                    sociedad.save()
            elif sociedad.fecha_comienzo_legales:
                fecha_fin = sociedad.fecha_comienzo_legales + timedelta(days=7)
                if date.today() > fecha_fin:
                    confirmar_sociedad_automatica(sociedad, User.objects.get(usuario_bonita="UsuarioLegalesAuto1"))


        return get_object_or_404(Sociedad.objects.all(), pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(SociedadVer, self).get_context_data(**kwargs)
        context['url_cancelar'] = reverse_lazy('sociedades:listar')
        context['logs'] = self.object.logs.all().select_related('usuario')
        return context


class Cerrar(LoginRequiredMixin, EsApoderadoMixin, ConfirmView):
    model = Sociedad
    template_name = 'confirmar_standar.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Sociedad,
                                 pk=self.kwargs['pk'],
                                 estado=Sociedad.PENDIENTE,
                                 creador=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(Cerrar, self).get_context_data(**kwargs)
        context['title'] = 'Cerrar Cambios Sociedad'
        context['url_cancelar'] = self.get_success_url()
        return context

    def confirm(self, request, *args, **kwargs):
        self.object = self.get_object()
        conectorAPIBonita = ConectorAPIBonita()
        conectorAPIBonita.actualizar_variables(self.request.user, self.object.id_caso, self.object.nombre, self.object.fecha_creacion, self.object.email, self.object.domicilio_real, self.object.domicilio_legal, self.object.socio.all(), self.object.paises_exportados.all())
        if not self.object.tramite_corregir_verificacion and not self.object.tramite_corregir_validacion:
            if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Completar formulario de registro', self.object.id_caso):
                self.object.datos_proceso.fecha_inicio_mesa_entrada = timezone.now()
                self.object.datos_proceso.save()
                self.object.estado = Sociedad.CERRADA
                self.object.save()
                self.object.logs.create(usuario=self.request.user, descripcion='Sociedad Cerrada')
                messages.success(self.request, 'Se cerraron correctamente los cambios de la sociedad')
            else:
                messages.error(self.request, 'Ocurrió un error con la API de Bonita')
        elif self.object.tramite_corregir_verificacion:
            if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Corrección de errores', self.object.id_caso):
                self.object.estado = Sociedad.CERRADA
                self.object.save()
                time.sleep(3)
                if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Completar formulario de registro', self.object.id_caso):
                    self.object.logs.create(usuario=self.request.user, descripcion='Sociedad Corregida y Cerrada')
                    messages.success(self.request, 'Se cerraron correctamente los cambios de la sociedad')
                else:
                    messages.error(self.request, 'Ocurrió un error con la API de Bonita')
            else:
                messages.error(self.request, 'Ocurrió un error con la API de Bonita')
        else:
            conectorAPIBonita.actualizar_variable_long(self.request.user, self.object.id_caso, 'plazoValidacionTramite', 7*24*60*60)
            if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Corrección de trámite', self.object.id_caso):
                self.object.estado = Sociedad.REVISADA
                self.object.fecha_comienzo_legales = timezone.now()
                self.object.tramite_corregir_validacion = False
                self.object.save()
                self.object.logs.create(usuario=self.request.user, descripcion='Sociedad Corregida y Revisada')
                messages.success(self.request, 'Se cerraron correctamente los cambios de la sociedad')
            else:
                messages.error(self.request, 'Ocurrió un error con la API de Bonita')
        return super(Cerrar, self).confirm(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('sociedades:sociedad', kwargs={'pk': self.object.pk})


class Revisar(LoginRequiredMixin, EsEmpleadoMesaEntradaMixin, ConfirmView):
    model = Sociedad
    template_name = 'confirmar_standar.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Sociedad,
                                 pk=self.kwargs['pk'],
                                 estado=Sociedad.CERRADA)

    def get_context_data(self, **kwargs):
        context = super(Revisar, self).get_context_data(**kwargs)
        context['title'] = 'Marcar Sociedad Revisada'
        context['url_cancelar'] = self.get_success_url()
        return context

    def confirm(self, request, *args, **kwargs):
        self.object = self.get_object()
        conectorAPIBonita = ConectorAPIBonita()
        conectorAPIBonita.actualizar_variable(self.request.user, self.object.id_caso, 'formularioValido', 'True')
        conectorAPIBonita.actualizar_variable_long(self.request.user, self.object.id_caso, 'plazoValidacionTramite', 7*24*60*60)
        if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Verificación de formulario de registro', self.object.id_caso):
            revisar_sociedad(sociedad=self.object, operador=self.request.user)
            messages.success(self.request, 'Se marcó la sociedad como revisada')
        else:
            messages.error(self.request, 'Ocurrió un error con la API de Bonita')
        return super(Revisar, self).confirm(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('sociedades:sociedad', kwargs={'pk': self.object.pk})


class Rechazar(LoginRequiredMixin, EsEmpleadoMesaEntradaMixin, FormView):
    form_class = RechazarForm
    template_name = 'formulario_standar.html'

    def get_context_data(self, **kwargs):
        context = super(Rechazar, self).get_context_data(**kwargs)
        context['title'] = 'Volver Sociedad a Pendiente'
        context['url_cancelar'] = self.get_success_url()
        return context

    def form_valid(self, form):
        sociedad = get_object_or_404(Sociedad, 
                                pk=self.kwargs['pk'], 
                                estado=Sociedad.CERRADA)
        conectorAPIBonita = ConectorAPIBonita()
        conectorAPIBonita.actualizar_variable(self.request.user, sociedad.id_caso, 'formularioValido', 'False')
        conectorAPIBonita.actualizar_variable_long(self.request.user, sociedad.id_caso, 'plazoCorreccionFormulario', form.cleaned_data['plazo_correccion']*24*60*60)
        if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Verificación de formulario de registro', sociedad.id_caso):
            rechazar_sociedad(sociedad=sociedad, operador=self.request.user, motivo=form.cleaned_data['motivo'], plazo=form.cleaned_data['plazo_correccion'])
            messages.success(self.request, 'Se marcó como pendiente la sociedad')
        else:
            messages.error(self.request, 'Ocurrió un error con la API de Bonita')
        return super(Rechazar, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('sociedades:sociedad', kwargs={'pk': self.kwargs['pk']})


class Confirmar(LoginRequiredMixin, EsEmpleadoAreaLegalesMixin, ConfirmView):
    model = Sociedad
    template_name = 'confirmar_standar.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Sociedad,
                                 pk=self.kwargs['pk'],
                                 estado=Sociedad.REVISADA)

    def get_context_data(self, **kwargs):
        context = super(Confirmar, self).get_context_data(**kwargs)
        context['title'] = 'Marcar Sociedad Confirmada'
        context['url_cancelar'] = self.get_success_url()
        return context

    # def confirm(self, request, *args, **kwargs):
    #     self.object = self.get_object()
    #     solicitar_estampillado(self.object, self.request.user)
    #     generar_qr(self.object)
    #     conectorAPIBonita = ConectorAPIBonita()
    #     conectorAPIBonita.actualizar_variable(self.request.user, self.object.id_caso, 'tramiteValido', 'True')
    #     conectorAPIBonita.ejecutar_tarea(self.request.user, 'Validación de trámite', self.object.id_caso)
    #     confirmar_sociedad(sociedad=self.object, operador=self.request.user)
    #     messages.success(self.request, 'Se marcó la sociedad como confirmada')
    #     return super(Confirmar, self).confirm(request, *args, **kwargs)
    def confirm(self, request, *args, **kwargs):
        self.object = self.get_object()
        conectorAPIBonita = ConectorAPIBonita()
        conectorAPIBonita.actualizar_variable(self.request.user, self.object.id_caso, 'tramiteValido', 'True')
        if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Validación de trámite', self.object.id_caso):
            solicitar_estampillado(self.object, self.request.user)
            generar_qr(self.object)
            confirmar_sociedad(sociedad=self.object, operador=self.request.user)
            messages.success(self.request, 'Se marcó la sociedad como confirmada')
        else:
            messages.error(self.request, 'Ocurrió un error con la API de Bonita')
        return super(Confirmar, self).confirm(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('sociedades:sociedad', kwargs={'pk': self.object.pk})


class RechazarConfirmacion(LoginRequiredMixin, EsEmpleadoAreaLegalesMixin, FormView):
    form_class = RechazarConfirmacionForm
    template_name = 'formulario_standar.html'

    def get_context_data(self, **kwargs):
        context = super(RechazarConfirmacion, self).get_context_data(**kwargs)
        context['title'] = 'Volver Sociedad a Pendiente'
        context['url_cancelar'] = self.get_success_url()
        return context

    def form_valid(self, form):
        sociedad = get_object_or_404(Sociedad, 
                                pk=self.kwargs['pk'], 
                                estado=Sociedad.REVISADA)
        conectorAPIBonita = ConectorAPIBonita()
        conectorAPIBonita.actualizar_variable(self.request.user, sociedad.id_caso, 'tramiteValido', 'False')
        if conectorAPIBonita.ejecutar_tarea(self.request.user, 'Validación de trámite', sociedad.id_caso):
            rechazar_confirmacion_sociedad(sociedad=sociedad, operador=self.request.user, motivo=form.cleaned_data['motivo'])
            messages.success(self.request, 'Se marcó como pendiente la sociedad')
        else:
            messages.error(self.request, 'Ocurrió un error con la API de Bonita')
        return super(RechazarConfirmacion, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('sociedades:sociedad', kwargs={'pk': self.kwargs['pk']})


class SociedadVerPublico(DetailView):
    model = Sociedad
    template_name = 'sociedad/ver_publico.html'
    context_object_name = 'sociedad'

    def get_object(self, queryset=None):
        return get_object_or_404(Sociedad.objects.all(), hash=self.kwargs['hash'])

    def get_context_data(self, **kwargs):
        context = super(SociedadVerPublico, self).get_context_data(**kwargs)
        context['url_cancelar'] = reverse_lazy('sociedades:listar')
        return context


class CarpetasGeneradas(LoginRequiredMixin, EsEmpleadoMesaEntradaMixin, ConfirmView):
    model = Sociedad
    template_name = 'confirmar_standar.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Sociedad,
                                 pk=self.kwargs['pk'],
                                 estado=Sociedad.CONFIRMADA)

    def get_context_data(self, **kwargs):
        context = super(CarpetasGeneradas, self).get_context_data(**kwargs)
        context['title'] = 'Marcar Sociedad con Carpetas Generadas'
        context['url_cancelar'] = self.get_success_url()
        return context

    def confirm(self, request, *args, **kwargs):
        self.object = self.get_object()
        carpetas_generadas = generar_carpetas(sociedad=self.object, operador=self.request.user)
        if (carpetas_generadas):
            conectorAPIBonita = ConectorAPIBonita()
            conectorAPIBonita.ejecutar_tarea(self.request.user, 'Generar carpetas', self.object.id_caso)
            messages.success(self.request, 'Se marcó que la sociedad cuenta con sus carpetas generadas')
        return super(CarpetasGeneradas, self).confirm(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('sociedades:sociedad', kwargs={'pk': self.object.pk})