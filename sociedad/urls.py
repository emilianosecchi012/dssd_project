from django.urls import path

from .resources import ContinenteMasExportadoApi, LenguajeMasExportadoApi, EstadosMasRegistradoApi, ContinenteNoExportadoApi, RechazadosApi, \
    CantidadEsperaApi, CantidadAutoaceptadosApi, CantidadDiasProcesoApi, CantidadDiasMesaEntradaApi, CantidadDiasAreaLegalesApi
from .views import Registrar, Listar, SociedadVer, SociedadVer, Editar, Cerrar, Revisar, Rechazar, Confirmar, RechazarConfirmacion, SociedadVerPublico, \
    CarpetasGeneradas

app_name = 'liquidacion'

urlpatterns = [
    path('registrar', Registrar.as_view(), name='registrar'),
    path('listar-sociedades', Listar.as_view(), name='listar'),
    path('sociedad/<int:pk>', SociedadVer.as_view(), name='sociedad'),
    path('editar/<int:pk>', Editar.as_view(), name='editar'),
    path('cerrar/<int:pk>', Cerrar.as_view(), name='cerrar'),
    path('revisar/<int:pk>', Revisar.as_view(), name='revisar'),
    path('rechazar/<int:pk>', Rechazar.as_view(), name='rechazar'),
    path('confirmar/<int:pk>', Confirmar.as_view(), name='confirmar'),
    path('rechazar-confirmacion/<int:pk>', RechazarConfirmacion.as_view(), name='rechazar_confirmacion'),
    path('publica/<str:hash>', SociedadVerPublico.as_view(), name='sociedad_publica'),
    path('carpetas-generadas/<int:pk>', CarpetasGeneradas.as_view(), name='carpetas_generadas'),

    #API
    path('continentes-mas-exportados', ContinenteMasExportadoApi.as_view(), name='continentes_mas_exportados'),
    path('lenguaje-mas-exportados', LenguajeMasExportadoApi.as_view(), name='lenguaje_mas_exportados'),
    path('estados-mas-registrados', EstadosMasRegistradoApi.as_view(), name='estados_mas_registrados'),
    path('continentes-no-exportados', ContinenteNoExportadoApi.as_view(), name='continentes_no_exportados'),

    #API DATOS
    path('rechazados', RechazadosApi.as_view(), name='rechazados'),
    path('cantidad-espera', CantidadEsperaApi.as_view(), name='cantidad_espera'),
    path('cantidad-autoaceptados', CantidadAutoaceptadosApi.as_view(), name='cantidad_autoaceptados'),
    path('cantidad-dias-proceso', CantidadDiasProcesoApi.as_view(), name='cantidad_dias_proceso'),
    path('cantidad-dias-mesa-entrada', CantidadDiasMesaEntradaApi.as_view(), name='cantidad_dias_mesa_entrada'),
    path('cantidad-dias-area-legales', CantidadDiasAreaLegalesApi.as_view(), name='cantidad_dias_area_legales'),
]
