import requests

from django.db.models import Count, Sum, F, IntegerField, ExpressionWrapper
from django.db.models.functions import Cast
from django.views import View
from django.http import JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import PaisEstado, Sociedad, Domicilio, DatosProceso

class ContinenteMasExportadoApi(View):
    def get(self, request):
        query = '''query {
                continents {
                    name
                    code
                    countries {
                        code
                    }
                }
            }
        '''
        request = requests.post('https://countries.trevorblades.com/', json={'query': query})
        if request.status_code != 200:
            return JsonResponse('Error')
        sociedades = Sociedad.objects.all().prefetch_related('paises_exportados')
        exportados = PaisEstado.objects.none()
        for sociedad in sociedades:
            exportados = exportados | sociedad.paises_exportados.all()
        contador_continentes = {}
        resultado_continentes = {'continents': []}
        continentes_codigos = {}
        for continente in request.json()['data']['continents']:
            contador_continentes[continente['name']] = 0
            #resultado_continentes['continents'].append({'name': continente['name'], 'code': continente['code']})
            continentes_codigos[continente['name']] = continente['code']
        for exportado in exportados:
            for continente in request.json()['data']['continents']:
                paises = continente['countries']
                exportado_diccionario = {'code': exportado.pais_codigo}
                if exportado_diccionario in paises:
                    contador_continentes[continente['name']]+=1
        for k in sorted(contador_continentes, key=contador_continentes.get, reverse=True):
            resultado_continentes['continents'].append({'name': k, 'code': continentes_codigos[k], 'quantity': contador_continentes[k]})
            
        return JsonResponse(resultado_continentes)


class LenguajeMasExportadoApi(View):
    def get(self, request):
        sociedades = Sociedad.objects.all().prefetch_related('paises_exportados')
        exportados = PaisEstado.objects.none()
        for sociedad in sociedades:
            exportados = exportados | sociedad.paises_exportados.all()
        mas_exportados = exportados.values('pais_codigo').annotate(total=Count('pais_codigo')).order_by('-total')[:5]
        resultado = {'countries': []}
        for pais in mas_exportados:
            query = '''query {
                    country (code: "'''+pais['pais_codigo']+'''"){
                        name
                        code
                        languages {
                            code
                            name
                        }
                    }
                }
            '''
            request = requests.post('https://countries.trevorblades.com/', json={'query': query})
            if request.status_code != 200:
                return JsonResponse({'Error':'Error'})
            resultado['countries'].append(request.json()['data']['country'])
        return JsonResponse(resultado)


class EstadosMasRegistradoApi(View):
    def get(self, request):
        registrados = Domicilio.objects.exclude(sociedad_dom_leg=None)
        mas_registrados = registrados.values('pais_estado__estado_descripcion', 'pais_estado__pais_codigo').annotate(total=Count('pais_estado__estado_descripcion')).order_by('-total')[:5]

        resultado = {'states': []}
        for estado in mas_registrados:
            query = '''query {
                    country (code: "'''+estado['pais_estado__pais_codigo']+'''"){
                        name
                        code
                        states {
                            code
                            name
                        }
                    }
                }
            '''
            request = requests.post('https://countries.trevorblades.com/', json={'query': query})
            if request.status_code != 200:
                return JsonResponse({'Error':'Error'})
            resultado['states'].append(
                {'country':
                    {'code':request.json()['data']['country']['code'], 'name': request.json()['data']['country']['name']}, 
                    'code':list(filter(lambda estad: estad['name'] == estado['pais_estado__estado_descripcion'], request.json()['data']['country']['states']))[0]['code'],
                    'name':estado['pais_estado__estado_descripcion']
                })

        return JsonResponse(resultado)

class ContinenteNoExportadoApi(View):
    def get(self, request):
        query = '''query {
                continents {
                    name
                    code
                    countries {
                        code
                    }
                }
            }
        '''
        request = requests.post('https://countries.trevorblades.com/', json={'query': query})
        if request.status_code != 200:
            return JsonResponse('Error')
        sociedades = Sociedad.objects.all().prefetch_related('paises_exportados')
        exportados = PaisEstado.objects.none()
        for sociedad in sociedades:
            exportados = exportados | sociedad.paises_exportados.all()
        contador_continentes = {}
        resultado_continentes = {'continents': []}
        continentes_codigos = {}
        for continente in request.json()['data']['continents']:
            contador_continentes[continente['name']] = 0
            #resultado_continentes['continents'].append({'name': continente['name'], 'code': continente['code']})
            continentes_codigos[continente['name']] = continente['code']
        for exportado in exportados:
            for continente in request.json()['data']['continents']:
                paises = continente['countries']
                exportado_diccionario = {'code': exportado.pais_codigo}
                if exportado_diccionario in paises:
                    contador_continentes[continente['name']]+=1
        for k in sorted(contador_continentes, key=contador_continentes.get, reverse=True):
            if contador_continentes[k] == 0:
                resultado_continentes['continents'].append({'name': k, 'code': continentes_codigos[k], 'quantity': contador_continentes[k]})
            
        return JsonResponse(resultado_continentes)


class RechazadosApi(LoginRequiredMixin, View):
    def get(self, request):
        rechazados_entrada = DatosProceso.objects.all().aggregate(Sum('cantidad_rechazos_mesa_entrada'))['cantidad_rechazos_mesa_entrada__sum']
        rechazados_legales = DatosProceso.objects.all().aggregate(Sum('cantidad_rechazos_area_legales'))['cantidad_rechazos_area_legales__sum']
        rechazados_entrada = 0 if not rechazados_entrada else rechazados_entrada
        rechazados_legales = 0 if not rechazados_legales else rechazados_legales
        dic = {
            'Rechazados Mesa de Entrada': rechazados_entrada,
            'Rechazados Area de Legales': rechazados_legales,
        }

        series = [serie for serie in dic.values()]

        default_labels = ['Rechazados Mesa de Entrada', 'Rechazados Area de Legales']
        default_items = series
        data = {
            "labels": default_labels,
            "items": default_items,
        }
        return JsonResponse(data)


class CantidadEsperaApi(LoginRequiredMixin, View):
    def get(self, request):
        default_items = Sociedad.objects.filter(estado=Sociedad.CERRADA) | Sociedad.objects.filter(estado=Sociedad.REVISADA) | \
        Sociedad.objects.filter(estado=Sociedad.CONFIRMADA)
        default_items = default_items.values_list('estado').annotate(cnt_estado=Count('estado')).order_by('estado')
        
        dic = {
            'Cerrada': [0],
            'Revisada': [0],
            'Confirmada': [0],
        }

        for estado in default_items:
            dic[estado[0]] = [estado[1]]

        series = [serie for serie in dic.values()]

        default_labels = ['Espera en mesa de entrada', 'Espera en area de legales', 'Espera por carpetas']
        default_items = series
        data = {
            "labels": default_labels,
            "items": default_items,
        }

        return JsonResponse(data)


class CantidadAutoaceptadosApi(LoginRequiredMixin, View):
    def get(self, request):
        autoaceptados = DatosProceso.objects.filter(autoaceptado=True).count()
        no_autoaceptados = DatosProceso.objects.filter(autoaceptado=False).count()
        autoaceptados = 0 if not autoaceptados else autoaceptados
        no_autoaceptados = 0 if not no_autoaceptados else no_autoaceptados
        dic = {
            'Autoaceptados': autoaceptados,
            'Aceptados por empleado': no_autoaceptados,
        }

        series = [serie for serie in dic.values()]

        default_labels = ['Autoaceptados', 'Aceptados por empleado']
        default_items = series
        data = {
            "labels": default_labels,
            "items": default_items,
        }
        return JsonResponse(data)


class CantidadDiasProcesoApi(LoginRequiredMixin, View):
    def get(self, request):
        default_values = DatosProceso.objects.exclude(fecha_fin_proceso=None)
        default_values = default_values.annotate(cantidad_dias_proceso=ExpressionWrapper(Cast(F('fecha_fin_proceso')-F('fecha_inicio_proceso'), IntegerField()) / 86400000000, output_field=IntegerField()))
        
        dic = {
            'Mismo día': default_values.filter(cantidad_dias_proceso=0).count(),
            '1 día': default_values.filter(cantidad_dias_proceso=1).count(),
            '2 días': default_values.filter(cantidad_dias_proceso=2).count(),
            '3 días': default_values.filter(cantidad_dias_proceso=3).count(),
            '4 días': default_values.filter(cantidad_dias_proceso=4).count(),
            '5 días': default_values.filter(cantidad_dias_proceso=5).count(),
            '6 días': default_values.filter(cantidad_dias_proceso=6).count(),
            '7 días': default_values.filter(cantidad_dias_proceso=7).count(),
            'Más de 7 días': default_values.filter(cantidad_dias_proceso__gt=7).count(),
        }

        series = [serie for serie in dic.values()]

        default_labels = ['Mismo día', '1 día', '2 días', '3 días', '4 días', '5 días', '6 días', '7 días', 'Más de7 días']
        default_items = series
        data = {
            "labels": default_labels,
            "items": default_items,
        }
        return JsonResponse(data)


class CantidadDiasMesaEntradaApi(LoginRequiredMixin, View):
    def get(self, request):
        default_values = DatosProceso.objects.exclude(fecha_fin_proceso=None)
        default_values = default_values.annotate(cantidad_dias_proceso=ExpressionWrapper(Cast(F('fecha_fin_mesa_entrada')-F('fecha_inicio_mesa_entrada'), IntegerField()) / 86400000000, output_field=IntegerField()))
        
        dic = {
            'Mismo día': default_values.filter(cantidad_dias_proceso=0).count(),
            '1 día': default_values.filter(cantidad_dias_proceso=1).count(),
            '2 días': default_values.filter(cantidad_dias_proceso=2).count(),
            '3 días': default_values.filter(cantidad_dias_proceso=3).count(),
            '4 días': default_values.filter(cantidad_dias_proceso=4).count(),
            '5 días': default_values.filter(cantidad_dias_proceso=5).count(),
            '6 días': default_values.filter(cantidad_dias_proceso=6).count(),
            '7 días': default_values.filter(cantidad_dias_proceso=7).count(),
            'Más de 7 días': default_values.filter(cantidad_dias_proceso__gt=7).count(),
        }

        series = [serie for serie in dic.values()]

        default_labels = ['Mismo día', '1 día', '2 días', '3 días', '4 días', '5 días', '6 días', '7 días', 'Más de7 días']
        default_items = series
        data = {
            "labels": default_labels,
            "items": default_items,
        }
        return JsonResponse(data)


class CantidadDiasAreaLegalesApi(LoginRequiredMixin, View):
    def get(self, request):
        default_values = DatosProceso.objects.exclude(fecha_fin_proceso=None)
        default_values = default_values.annotate(cantidad_dias_proceso=ExpressionWrapper(Cast(F('fecha_fin_area_legales')-F('fecha_inicio_area_legales'), IntegerField()) / 86400000000, output_field=IntegerField()))
        
        dic = {
            'Mismo día': default_values.filter(cantidad_dias_proceso=0).count(),
            '1 día': default_values.filter(cantidad_dias_proceso=1).count(),
            '2 días': default_values.filter(cantidad_dias_proceso=2).count(),
            '3 días': default_values.filter(cantidad_dias_proceso=3).count(),
            '4 días': default_values.filter(cantidad_dias_proceso=4).count(),
            '5 días': default_values.filter(cantidad_dias_proceso=5).count(),
            '6 días': default_values.filter(cantidad_dias_proceso=6).count(),
            '7 días': default_values.filter(cantidad_dias_proceso=7).count(),
            'Más de 7 días': default_values.filter(cantidad_dias_proceso__gt=7).count(),
        }

        series = [serie for serie in dic.values()]

        default_labels = ['Mismo día', '1 día', '2 días', '3 días', '4 días', '5 días', '6 días', '7 días', 'Más de7 días']
        default_items = series
        data = {
            "labels": default_labels,
            "items": default_items,
        }
        return JsonResponse(data)