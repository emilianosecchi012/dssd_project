from django.contrib import admin
from .models import Sociedad, Log, Domicilio, Socio, PaisEstado, DatosProceso


class LogInline(admin.TabularInline):
    model = Log
    fields = (
        'usuario',
        'descripcion',
    )
    raw_id_fields = ('usuario', )


class SociedadAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'nombre',
        'fecha_creacion',
        'email',
    )
    search_fields = (
        'nombre',
        'fecha_creacion',
        'email',
    )
    inlines = (
        LogInline,
    )


admin.site.register(Sociedad, SociedadAdmin)
admin.site.register(Socio)
admin.site.register(PaisEstado)
admin.site.register(Domicilio)
admin.site.register(DatosProceso)