from django import forms
from django.core.exceptions import ValidationError
from django.forms import formset_factory, BaseFormSet
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field

from core.mixins import FormHelperMixin
from core.widgets import CalendarioWidget
from .models import Socio
import os


class SociedadForm(forms.Form):
    nombre = forms.CharField(max_length=250)
    fecha_creacion = forms.DateField(widget=CalendarioWidget())
    archivo_estatuto = forms.FileField()
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(SociedadForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout()

        for key in self.fields.keys():
            self.helper.layout.fields.append(Field(key))

    class Media:
        js = ('js/jquery.formset.b4.js',)

    def clean_archivo_estatuto(self):
        archivo = self.cleaned_data['archivo_estatuto']
        extensiones = ['.pdf', '.odt', '.docx']
        if os.path.splitext(archivo.name)[1] not in extensiones:
            msg = 'La extensión del archivo no es correcta. Debe seleccionar un .pdf, .odt o .docx'
            self.add_error('archivo_estatuto', msg)
        return archivo


class SociedadEditarForm(forms.Form):
    nombre = forms.CharField(max_length=250)
    fecha_creacion = forms.DateField(widget=CalendarioWidget())
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(SociedadEditarForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout()

        for key in self.fields.keys():
            self.helper.layout.fields.append(Field(key))

    class Media:
        js = ('js/jquery.formset.b4.js',)

    def clean_archivo_estatuto(self):
        archivo = self.cleaned_data['archivo_estatuto']
        extensiones = ['.pdf', '.odt', '.docx']
        if os.path.splitext(archivo.name)[1] not in extensiones:
            msg = 'La extensión del archivo no es correcta. Debe seleccionar un .pdf, .odt o .docx'
            self.add_error('archivo_estatuto', msg)
        return archivo

class PaisEstadoFormSetBase(BaseFormSet):

    def __init__(self, *args, **kwargs):
        super(PaisEstadoFormSetBase, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.template = 'bootstrap4/table_inline_formset.html'

    def _construct_form(self, i, **kwargs):
        form = super(PaisEstadoFormSetBase, self)._construct_form(i, **kwargs)
        return form

    # def clean(self):
    #     for form in self.forms:
    #         try:
    #             pais = form.cleaned_data.get('pais')
    #             if pais is None:
    #                 raise forms.ValidationError('No se eligió una país.')
    #             estado = form.cleaned_data.get('estado')
    #             if estado is None:
    #                 raise forms.ValidationError('No se eligió un estado.')
    #         except TypeError:
    #             del form


class PaisEstadoForm(forms.Form):
    pais = forms.CharField(widget=forms.Select(choices=[]), required=False)
    estado = forms.CharField(widget=forms.Select(choices=[]), required=False)


PaisEstadoFormSet = formset_factory(form=PaisEstadoForm,
                                    formset=PaisEstadoFormSetBase,
                                    extra=1,
                                    can_delete=True)


class DomicilioLegalFormSetBase(BaseFormSet):

    def __init__(self, *args, **kwargs):
        super(DomicilioLegalFormSetBase, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.template = 'bootstrap4/domicilio_legal_formset.html'

    def _construct_form(self, i, **kwargs):
        form = super(DomicilioLegalFormSetBase, self)._construct_form(i, **kwargs)
        form.fields['pais_dom_legal'].label = "Pais"
        form.fields['estado_dom_legal'].label = "Estado"
        return form


class DomicilioLegalForm(forms.Form):
    pais_dom_legal = forms.CharField(widget=forms.Select(choices=[]))
    estado_dom_legal = forms.CharField(widget=forms.Select(choices=[]))
    localidad = forms.CharField(max_length=100)
    codigo_postal = forms.CharField(max_length=100)
    calle = forms.CharField(max_length=100)
    numero = forms.CharField(max_length=100)
    piso = forms.CharField(max_length=100, required=False)
    departamento = forms.CharField(max_length=100, required=False)


DomicilioLegalFormSet = formset_factory(form=DomicilioLegalForm,
                                        formset=DomicilioLegalFormSetBase,
                                        extra=1,
                                        can_delete=True)


class DomicilioRealFormSetBase(BaseFormSet):

    def __init__(self, *args, **kwargs):
        super(DomicilioRealFormSetBase, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.template = 'bootstrap4/domicilio_real_formset.html'

    def _construct_form(self, i, **kwargs):
        form = super(DomicilioRealFormSetBase, self)._construct_form(i, **kwargs)
        form.fields['pais_dom_real'].label = "Pais"
        form.fields['estado_dom_real'].label = "Estado"
        return form


class DomicilioRealForm(forms.Form):
    pais_dom_real = forms.CharField(widget=forms.Select(choices=[]))
    estado_dom_real = forms.CharField(widget=forms.Select(choices=[]))
    localidad = forms.CharField(max_length=100)
    codigo_postal = forms.CharField(max_length=100)
    calle = forms.CharField(max_length=100)
    numero = forms.CharField(max_length=100)
    piso = forms.CharField(max_length=100, required=False)
    departamento = forms.CharField(max_length=100, required=False)


DomicilioRealFormSet = formset_factory(form=DomicilioRealForm,
                                        formset=DomicilioRealFormSetBase,
                                        extra=1,
                                        can_delete=True)


class SocioFormSetBase(BaseFormSet):

    def __init__(self, *args, **kwargs):
        super(SocioFormSetBase, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.template = 'bootstrap4/socio_formset.html'

    def _construct_form(self, i, **kwargs):
        form = super(SocioFormSetBase, self)._construct_form(i, **kwargs)
        form.fields['aporte'].helper_text = "Ingresar porcentaje como decimal (50.00)"
        return form

    def clean(self):
        if any(self.errors):
            return
        aportes = 0
        existe_apoderado = False
        for form in self.forms:
            if self.can_delete and self._should_delete_form(form):
                continue
            aportes += form.cleaned_data.get('aporte')
            if aportes > 100:
                raise ValidationError("La suma de los aportes no puede superar el %100")
            apoderado = form.cleaned_data.get('apoderado')
            if apoderado == 'S':
                if existe_apoderado == True:
                    raise ValidationError("Solo puede haber un apoderado por sociedad")
                else:
                    existe_apoderado = True
        if aportes != 100:
            raise ValidationError("La suma de los aportes debe ser del %100")
        if existe_apoderado == False:
            raise ValidationError("Debe indicar al menos un apoderado")
            

class SocioForm(forms.Form):
    nombres = forms.CharField(max_length=250)
    apellidos = forms.CharField(max_length=250)
    aporte = forms.DecimalField(max_digits=8, decimal_places=2, max_value=100.00)
    apoderado = forms.ChoiceField(choices=Socio.SINO)


SocioFormSet = formset_factory(form=SocioForm,
                                formset=SocioFormSetBase,
                                extra=1,
                                can_delete=True)


class RechazarForm(FormHelperMixin, forms.Form):
    motivo = forms.CharField(widget=forms.Textarea)
    plazo_correccion = forms.IntegerField()
    
class RechazarConfirmacionForm(FormHelperMixin, forms.Form):
    motivo = forms.CharField(widget=forms.Textarea)