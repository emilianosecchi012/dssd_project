from django.conf import settings
from django.core.files.base import ContentFile
from django.core.mail import send_mail
from django.utils import timezone
from random import randint
from utils import generar_expediente_pdf
from google_drive_api.google_drive_conector import GoogleDriveAPIConector
from datetime import date
import requests
import http.client
import os

from .models import DatosProceso, PaisEstado, Domicilio, Sociedad, Socio


def crear_sociedad(usuario, nombre, fecha_creacion, archivo_estatuto, email, domicilio_real, domicilio_legal, socios, exportaciones):
    # CREO EXPORTACIONES
    lista_paises = []
    for e in exportaciones:
        query = '''
                query {
                    country (code: "'''+e['pais_codigo']+'''"){
                        name
                    }
                }
            '''
        request = requests.post('https://countries.trevorblades.com/', json={'query': query})
        if request.status_code != 200:
            return None
        pais_descripcion = request.json()['data']['country']['name']
        obj, existe = PaisEstado.objects.get_or_create(pais_codigo=e['pais_codigo'], estado_descripcion=e['estado_descripcion'], defaults={'pais_descripcion':pais_descripcion})
        lista_paises.append(obj)

    # CREO DOMICILIO LEGAL
    query = '''
            query {
                country (code: "'''+domicilio_legal['pais_dom_legal']+'''"){
                    name
                }
            }
        '''
    request = requests.post('https://countries.trevorblades.com/', json={'query': query})
    if request.status_code != 200:
        return None
    pais_descripcion = request.json()['data']['country']['name']
    pais_dom_legal, existe = PaisEstado.objects.get_or_create(pais_codigo=domicilio_legal['pais_dom_legal'], estado_descripcion=domicilio_legal['estado_dom_legal'], defaults={'pais_descripcion':pais_descripcion})
    instancia_domicilio_legal = Domicilio.objects.create(pais_estado=pais_dom_legal, localidad=domicilio_legal['localidad'], calle=domicilio_legal['calle'],
        numero=domicilio_legal['numero'], piso=domicilio_legal['piso'], departamento=domicilio_legal['departamento'], codigo_postal=domicilio_legal['codigo_postal'])

    # CREO DOMICILIO REAL
    query = '''
            query {
                country (code: "'''+domicilio_real['pais_dom_real']+'''"){
                    name
                }
            }
        '''
    request = requests.post('https://countries.trevorblades.com/', json={'query': query})
    if request.status_code != 200:
        return None
    pais_descripcion = request.json()['data']['country']['name']
    pais_dom_real, existe = PaisEstado.objects.get_or_create(pais_codigo=domicilio_real['pais_dom_real'], estado_descripcion=domicilio_real['estado_dom_real'], defaults={'pais_descripcion':pais_descripcion})
    instancia_domicilio_real = Domicilio.objects.create(pais_estado=pais_dom_real, localidad=domicilio_real['localidad'], calle=domicilio_real['calle'],
        numero=domicilio_real['numero'], piso=domicilio_real['piso'], departamento=domicilio_real['departamento'], codigo_postal=domicilio_real['codigo_postal'])

    # CREO SOCIEDAD
    sociedad = Sociedad.objects.create(creador=usuario, nombre=nombre, fecha_creacion=fecha_creacion, archivo_estatuto=archivo_estatuto, email=email, 
        domicilio_legal=instancia_domicilio_legal, domicilio_real=instancia_domicilio_real)
    for pais in lista_paises:
        sociedad.paises_exportados.add(pais)

    # CREO DATOS DEL PROCESO
    sociedad.datos_proceso = DatosProceso.objects.create()
    sociedad.save()

    # CREO SOCIOS
    for s in socios:
        Socio.objects.create(nombres=s['nombres'], apellidos=s['apellidos'], aporte=s['aporte'], apoderado=s['apoderado'], sociedad=sociedad)

    return sociedad


def editar_sociedad(usuario, sociedad, nombre, fecha_creacion, email, domicilio_real, domicilio_legal, socios, exportaciones):
    # CREO EXPORTACIONES
    lista_paises = []
    for e in exportaciones:
        query = '''
                query {
                    country (code: "'''+e['pais_codigo']+'''"){
                        name
                    }
                }
            '''
        request = requests.post('https://countries.trevorblades.com/', json={'query': query})
        if request.status_code != 200:
            return None
        pais_descripcion = request.json()['data']['country']['name']
        obj, existe = PaisEstado.objects.get_or_create(pais_codigo=e['pais_codigo'], estado_descripcion=e['estado_descripcion'], defaults={'pais_descripcion':pais_descripcion})
        lista_paises.append(obj)

    sociedad.paises_exportados.clear()
    for pais in lista_paises:
        sociedad.paises_exportados.add(pais)

    # CREO DOMICILIO LEGAL
    query = '''
            query {
                country (code: "'''+domicilio_legal['pais_dom_legal']+'''"){
                    name
                }
            }
        '''
    request = requests.post('https://countries.trevorblades.com/', json={'query': query})
    if request.status_code != 200:
        return None
    pais_descripcion = request.json()['data']['country']['name']
    pais_dom_legal, existe = PaisEstado.objects.get_or_create(pais_codigo=domicilio_legal['pais_dom_legal'], estado_descripcion=domicilio_legal['estado_dom_legal'], defaults={'pais_descripcion':pais_descripcion})
    instancia_domicilio_legal = Domicilio.objects.create(pais_estado=pais_dom_legal, localidad=domicilio_legal['localidad'], calle=domicilio_legal['calle'],
        numero=domicilio_legal['numero'], piso=domicilio_legal['piso'], departamento=domicilio_legal['departamento'], codigo_postal=domicilio_legal['codigo_postal'])
    
    sociedad.domicilio_legal = instancia_domicilio_legal

    # CREO DOMICILIO REAL
    query = '''
            query {
                country (code: "'''+domicilio_real['pais_dom_real']+'''"){
                    name
                }
            }
        '''
    request = requests.post('https://countries.trevorblades.com/', json={'query': query})
    if request.status_code != 200:
        return None
    pais_descripcion = request.json()['data']['country']['name']
    pais_dom_real, existe = PaisEstado.objects.get_or_create(pais_codigo=domicilio_real['pais_dom_real'], estado_descripcion=domicilio_real['estado_dom_real'], defaults={'pais_descripcion':pais_descripcion})
    instancia_domicilio_real = Domicilio.objects.create(pais_estado=pais_dom_real, localidad=domicilio_real['localidad'], calle=domicilio_real['calle'],
        numero=domicilio_real['numero'], piso=domicilio_real['piso'], departamento=domicilio_real['departamento'], codigo_postal=domicilio_real['codigo_postal'])

    sociedad.domicilio_real = instancia_domicilio_real

    # CREO DATOS DEL PROCESO
    #sociedad.datos_proceso = DatosProceso.objects.create()
    sociedad.nombre = nombre
    sociedad.fecha_creacion = fecha_creacion
    sociedad.email = email

    # CREO SOCIOS
    for socio in sociedad.socio.all():
        socio.delete()
    sociedad.save()
    for s in socios:
        Socio.objects.create(nombres=s['nombres'], apellidos=s['apellidos'], aporte=s['aporte'], apoderado=s['apoderado'], sociedad=sociedad)

    return sociedad


def revisar_sociedad(sociedad, operador):
    asunto = 'Registro sociedad revisado'
    mensaje = 'Un empleado de mesa de entradas revisó y aceptó su registro de sociedad.'
    send_mail(asunto, mensaje, settings.EMAIL_HOST_USER, [sociedad.email])

    sociedad.nro_expediente = str(randint(10000000, 99999999))+str(sociedad.pk)
    sociedad.estado = Sociedad.REVISADA
    sociedad.fecha_rechazo = None
    sociedad.plazo_correccion = None
    sociedad.tramite_corregir_verificacion = False
    sociedad.fecha_comienzo_legales = timezone.now()
    sociedad.datos_proceso.fecha_fin_mesa_entrada = timezone.now()
    sociedad.datos_proceso.fecha_inicio_area_legales = timezone.now()
    sociedad.datos_proceso.save()
    sociedad.save()
    sociedad.logs.create(usuario=operador, descripcion='Sociedad Revisada')


def rechazar_sociedad(sociedad, operador, motivo, plazo):
    asunto = 'Registro sociedad rechazado'
    mensaje = 'Un empleado de mesa de entradas rechazó su registro de sociedad por el siguiente motivo: \n'+motivo+'\nSu registro volvió a marcarse como pendiente y puede editarse nuevamente por un plazo de '+str(plazo)+' días.'
    send_mail(asunto, mensaje, settings.EMAIL_HOST_USER, [sociedad.email])

    sociedad.estado = Sociedad.PENDIENTE
    sociedad.fecha_rechazo = timezone.now()
    sociedad.plazo_correccion = plazo
    sociedad.tramite_corregir_verificacion = True
    sociedad.datos_proceso.cantidad_rechazos_mesa_entrada += 1
    sociedad.datos_proceso.save()
    sociedad.save()
    descripcion = 'Sociedad Rechazada: {}'.format(motivo)
    sociedad.logs.create(usuario=operador, descripcion=descripcion)


def confirmar_sociedad_automatica(sociedad, operador):
    sociedad.datos_proceso.autoaceptado = True
    sociedad.datos_proceso.save()
    confirmar_sociedad(sociedad, operador)


def confirmar_sociedad(sociedad, operador):
    asunto = 'Trámite de registro de sociedad confirmado'
    mensaje = 'Un empleado del área de legales revisó y aceptó su trámite de registro de sociedad.'
    send_mail(asunto, mensaje, settings.EMAIL_HOST_USER, [sociedad.email])

    sociedad.estado = Sociedad.CONFIRMADA
    sociedad.tramite_corregir_validacion = False
    sociedad.fecha_comienzo_legales = None
    if not sociedad.datos_proceso.autoaceptado:
        sociedad.datos_proceso.autoaceptado = False
    sociedad.datos_proceso.fecha_fin_area_legales = timezone.now()
    sociedad.datos_proceso.save()
    sociedad.save()
    sociedad.logs.create(usuario=operador, descripcion='Sociedad Confirmada')


def rechazar_confirmacion_sociedad(sociedad, operador, motivo):
    asunto = 'Confirmación de registro de sociedad rechazada'
    mensaje = 'Un empleado del área de legales rechazó su trámite de registro de sociedad por el siguiente motivo: \n'+motivo+'\nSu registro volvió a marcarse como pendiente y puede editarse nuevamente.'
    send_mail(asunto, mensaje, settings.EMAIL_HOST_USER, [sociedad.email])

    sociedad.estado = Sociedad.PENDIENTE
    sociedad.tramite_corregir_validacion = True
    sociedad.fecha_comienzo_legales = None
    sociedad.datos_proceso.cantidad_rechazos_area_legales += 1
    sociedad.datos_proceso.save()
    sociedad.save()
    descripcion = 'Confirmación de Trámite de Sociedad Rechazado: {}'.format(motivo)
    sociedad.logs.create(usuario=operador, descripcion=descripcion)


def solicitar_estampillado(sociedad, usuario):
    url_auth = 'http://dssd-ws-estampillado.herokuapp.com/auth'
    sesion = requests.Session()
    response = sesion.post(url_auth, json={'username': usuario.usuario_estampillado, 'password': usuario.password_estampillado})
    token = response.json()['token']
    url_estampillado = 'http://dssd-ws-estampillado.herokuapp.com/WSEstampillado/estampillado'
    response = sesion.post(url_estampillado, headers={'Authorization': 'Bearer '+token}, json={'nuExpediente': sociedad.nro_expediente})
    sociedad.hash = str(response.content).split(':')[1].replace(' ', '').replace("'",'')
    sociedad.save()


def generar_qr(sociedad):
    conn = http.client.HTTPSConnection("neutrinoapi-qr-code.p.rapidapi.com")

    headersList = {
    "Accept": "/",
    "User-Agent": "Thunder Client (https://www.thunderclient.io)",
    "content-type": "application/x-www-form-urlencoded",
    "x-rapidapi-host": "neutrinoapi-qr-code.p.rapidapi.com",
    "x-rapidapi-key": "5a9427721dmsheb9e0fce3cdb7dbp154d58jsn112c85b6911c" 
    }

    payload = ""

    conn.request("POST", "/qr-code?content=http%3A%2F%2F127.0.0.1%3A8000%2Fsociedad%2fpublica%2f"+sociedad.hash+"&width=128&height=128&fg-color=%23000000&bg-color=%23ffffff", payload, headersList)
    response = conn.getresponse() # Obtiene el objeto HTTPResponse
    result = response.read() # extrae el body
    path_imagen_qr = settings.MEDIA_ROOT + os.path.sep + "qrs" + os.path.sep + "qr_" + sociedad.hash + ".png"
    file = open(path_imagen_qr, "wb")
    file.write(result)
    file.close()
    sociedad.imagen_qr = '..' + os.path.sep + '..' + os.path.sep + "media" + os.path.sep + "qrs" + os.path.sep + "qr_" + sociedad.hash + ".png"
    sociedad.save()


def generar_carpetas(sociedad, operador):
    nombre_archivo_exp = "Expediente_Digital_" + sociedad.nombre + ".pdf"
    google_drive = GoogleDriveAPIConector()
    generar_expediente_pdf.generar_pdf(sociedad, date.today().strftime("%d/%m/%Y"), nombre_archivo_exp)
    url_archivo_digital = google_drive.subir_documento(nombre_archivo_exp)
    if (url_archivo_digital is not None):
        asunto = 'Carpetas de sociedad generadas'
        mensaje = 'Un empleado de mesa de entradas indicó que ya se encuentran disponibles las carpetas de su sociedad. Para ver el expediente digital, acceda al siguiente enlace: ' + url_archivo_digital
        send_mail(asunto, mensaje, settings.EMAIL_HOST_USER, [sociedad.email])
        sociedad.datos_proceso.fecha_fin_proceso = timezone.now()
        sociedad.datos_proceso.save()
        sociedad.estado = Sociedad.FINALIZADA
        sociedad.url_drive = url_archivo_digital
        sociedad.save()
        sociedad.logs.create(usuario=operador, descripcion='Sociedad con Carpetas Generadas')
        return True
    else:
        return False

class ConectorAPIBonita():
    token = None
    sesion = None
    id_caso = None

    @classmethod
    def login_bonita(self, username, password):
        url_auth = 'http://localhost:8080/bonita/loginservice'
        self.sesion = requests.Session()

        self.sesion.post(url_auth, data={'username': username, 'password': password, 'redirect': False})

        for cookie in self.sesion.cookies:
            if (cookie.name == "X-Bonita-API-Token"):
                self.token = cookie.value
                return True

    @classmethod
    def instanciar_caso(self, usuario):
        if not self.token:
            self.login_bonita(usuario.usuario_bonita, usuario.password_bonita)
        url_obtener_proceso = 'http://localhost:8080/bonita/API/bpm/process?s=registro_de_sociedades_anonimas'
        url_instanciacion = 'http://localhost:8080/bonita/API/bpm/case'
        response = self.sesion.get(url_obtener_proceso)
        id_proceso = response.json()[0]['id']
        payload = {'processDefinitionId': id_proceso}
        headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
        response = self.sesion.post(url_instanciacion, headers=headers, json=payload)
        print("Instanciar_caso")
        print(response.json())
        self.id_caso = response.json()['id']

        return self.id_caso

    @classmethod
    def buscar_tarea_humana(self, nombre, id_caso):
        url_buscar_tarea = 'http://localhost:8080/bonita/API/bpm/humanTask?f=name='+nombre+"&f=caseId="+id_caso
        headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
        response = self.sesion.get(url_buscar_tarea, headers=headers)
        print("buscar_tarea_humana")
        print(response.json())
        return response.json()[0]['id']

    @classmethod
    def buscar_id_usuario(self, usuario):
        url_buscar_id_usuario = 'http://localhost:8080/bonita/API/identity/user?f=userName='+usuario.usuario_bonita
        headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
        response = self.sesion.get(url_buscar_id_usuario, headers=headers)
        print("buscar_id_usuario")
        print(response.json())
        return response.json()[0]['id']

    @classmethod
    def asignar_usuario_tarea(self, usuario, nombre_tarea, id_caso):
        if not self.token:
            self.login_bonita(usuario.usuario_bonita, usuario.password_bonita)
        id_user = self.buscar_id_usuario(usuario)
        id_tarea = self.buscar_tarea_humana(nombre_tarea, id_caso)
        url_buscar_tarea = 'http://localhost:8080/bonita/API/bpm/humanTask/'+id_tarea
        payload = {'assigned_id': id_user}
        headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
        response = self.sesion.put(url_buscar_tarea, headers=headers, json=payload)
        print(response)
        return True

    @classmethod
    def ejecutar_tarea(self, usuario, nombre_tarea, id_caso):
        try:
            if not self.token:
                self.login_bonita(usuario.usuario_bonita, usuario.password_bonita)
            self.asignar_usuario_tarea(usuario, nombre_tarea, id_caso)
            id_tarea = self.buscar_tarea_humana(nombre_tarea, id_caso)
            url_buscar_tarea = 'http://localhost:8080/bonita/API/bpm/userTask/'+id_tarea+"/execution"
            headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
            response = self.sesion.post(url_buscar_tarea, headers=headers)
            print(response)
            return True
        except:
            return False


    @classmethod
    def actualizar_variables(self, usuario, id_caso, nombre, fecha_creacion, email, domicilio_real, domicilio_legal, socios, exportaciones):
        lista_str_socios = ""
        for s in socios:
            lista_str_socios += "[" + s.nombres + ", " + s.apellidos + ", " + str(s.aporte) + ", " + s.apoderado + "] "
        lista_str_exportaciones = ""
        for exp in exportaciones:
            lista_str_exportaciones += "[" + exp.pais_codigo + ", " + exp.estado_descripcion + "] "


        if not self.token:
            self.login_bonita(usuario.usuario_bonita, usuario.password_bonita)
        url_actualizar_variable = 'http://localhost:8080/bonita/API/bpm/caseVariable/'+id_caso+"/"
        headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
        payload = {
            "type": "java.lang.String",
            'value': nombre
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_nombreSociedad', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': fecha_creacion.strftime('%Y-%m-%d')
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_fechaCreacion', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': email
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_emailSociedad', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': str(domicilio_real.calle)
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domReal_calle', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': str(domicilio_real.numero)
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domReal_numero', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': str(domicilio_real.codigo_postal)
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domReal_codPostal', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': domicilio_real.pais_estado.estado_descripcion
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domReal_estado', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': domicilio_real.localidad
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domReal_localidad', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': domicilio_real.pais_estado.pais_descripcion
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domReal_pais', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': str(domicilio_legal.calle)
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domLegal_calle', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': str(domicilio_legal.numero)
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domLegal_numero', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': str(domicilio_legal.codigo_postal)
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domLegal_codPostal', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': domicilio_legal.pais_estado.estado_descripcion
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domLegal_estado', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': domicilio_legal.localidad
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domLegal_localidad', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': domicilio_legal.pais_estado.pais_descripcion
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_domLegal_pais', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': lista_str_socios
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_socios', headers=headers, json=payload)
        payload = {
            "type": "java.lang.String",
                    'value': lista_str_exportaciones
        }
        response = self.sesion.put(url_actualizar_variable+'formulario_paisesExportaciones', headers=headers, json=payload)

    @classmethod
    def actualizar_variable(self, usuario, id_caso, variable, booleano):
        if not self.token:
            self.login_bonita(usuario.usuario_bonita, usuario.password_bonita)
        url_actualizar_variable = 'http://localhost:8080/bonita/API/bpm/caseVariable/'+id_caso+"/"+variable
        headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
        payload = {
            "type": "java.lang.Boolean",
            'value': booleano
        }
        response = self.sesion.put(url_actualizar_variable, headers=headers, json=payload)
        # response = self.sesion.put(url_actualizar_variable+'formularioValido', headers=headers, json=payload)

    @classmethod
    def actualizar_variable_long(self, usuario, id_caso, variable, numero):
        if not self.token:
            self.login_bonita(usuario.usuario_bonita, usuario.password_bonita)
        url_actualizar_variable = 'http://localhost:8080/bonita/API/bpm/caseVariable/'+id_caso+"/"+variable
        headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
        payload = {
            "type": "java.lang.Long",
            'value': numero
        }
        response = self.sesion.put(url_actualizar_variable, headers=headers, json=payload)


    # @classmethod
    # def actualizar_variables_validacion(self, usuario, id_caso, bool):
    #     if not self.token:
    #         self.login_bonita(usuario.usuario_bonita, usuario.password_bonita)
    #     url_actualizar_variable = 'http://localhost:8080/bonita/API/bpm/caseVariable/'+id_caso+"/"
    #     headers = {'Content-Type': 'application/json', 'X-Bonita-API-Token': self.token}
    #     payload = {
    #         "type": "java.lang.Boolean",
    #         'value': bool
    #     }
    #     response = self.sesion.put(url_actualizar_variable+'tramiteValido', headers=headers, json=payload)