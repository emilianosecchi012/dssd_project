from django.db import models
from model_utils import Choices

from users.models import User


class PaisEstado(models.Model):
    pais_descripcion = models.CharField(max_length=250, null=True)
    pais_codigo = models.CharField(max_length=50, null=True)
    estado_descripcion = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        if self.estado_descripcion != '':
            return '{}, {}'.format(self.estado_descripcion, self.pais_descripcion)
        else:
            return '{}'.format(self.pais_descripcion)


class Domicilio(models.Model):
    pais_estado = models.ForeignKey(PaisEstado, on_delete=models.PROTECT, related_name='domicilios')
    localidad = models.CharField('Localidad', max_length=100)
    calle = models.CharField('Calle', max_length=100)
    numero = models.CharField('Número', max_length=100)
    piso = models.CharField('Piso', max_length=100, null=True, blank=True)
    departamento = models.CharField('Departamento', max_length=100, null=True, blank=True)
    codigo_postal = models.CharField('Código Postal', max_length=100)

    def __str__(self):
        return '{} Nro {}, {}'.format(self.calle, self.numero, self.localidad)


class DatosProceso(models.Model):
    fecha_inicio_proceso = models.DateTimeField(auto_now_add=True)
    fecha_fin_proceso = models.DateTimeField(null=True, blank=True)

    fecha_inicio_mesa_entrada = models.DateTimeField(null=True, blank=True)
    fecha_fin_mesa_entrada = models.DateTimeField(null=True, blank=True)

    fecha_inicio_area_legales = models.DateTimeField(null=True, blank=True)
    fecha_fin_area_legales = models.DateTimeField(null=True, blank=True)

    cantidad_rechazos_mesa_entrada = models.IntegerField(default=0)

    cantidad_rechazos_area_legales = models.IntegerField(default=0)

    autoaceptado = models.BooleanField(null=True)

    def __str__(self):
        return 'Datos métricas sociedad: {}'.format(self.sociedad)


class Sociedad(models.Model):
    PENDIENTE = 'Pendiente'
    CERRADA = 'Cerrada'
    REVISADA = 'Revisada'
    CONFIRMADA = 'Confirmada'
    ESTAMPADA = 'Estampada'
    FINALIZADA = 'Finalizada'
    CANCELADA = 'Cancelada'
    ESTADO = (
        (PENDIENTE, PENDIENTE),
        (CERRADA, CERRADA),
        (REVISADA, REVISADA),
        (CONFIRMADA, CONFIRMADA),
        (ESTAMPADA, ESTAMPADA),
        (FINALIZADA, FINALIZADA),
        (CANCELADA, CANCELADA),
    )
    creador = models.ForeignKey(User, on_delete=models.PROTECT, related_name='sociedad_duenio', null=True, blank=True)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    nombre = models.CharField(max_length=250)
    fecha_creacion = models.DateField()
    archivo_estatuto = models.FileField(upload_to="estatutos", null=True, blank=True)
    email = models.EmailField()
    estado = models.CharField(max_length=20, choices=ESTADO, default=PENDIENTE)
    paises_exportados = models.ManyToManyField(PaisEstado, related_name='sociedad')

    nro_expediente = models.CharField(max_length=250, null=True, blank=True)
    hash = models.CharField(max_length=250, null=True, blank=True)

    # Domicilio legal
    domicilio_legal = models.ForeignKey(Domicilio, on_delete=models.PROTECT, related_name='sociedad_dom_leg')

    # Domicilio real
    domicilio_real = models.ForeignKey(Domicilio, on_delete=models.PROTECT, related_name='sociedad_dom_rea')

    fecha_rechazo = models.DateField(null=True, blank=True)
    plazo_correccion = models.IntegerField(null=True, blank=True)
    tramite_corregir_verificacion = models.BooleanField(default=False)

    fecha_comienzo_legales = models.DateField(null=True, blank=True)
    tramite_corregir_validacion = models.BooleanField(default=False)

    id_caso = models.CharField(max_length=250, null=True, blank=True)

    imagen_qr = models.CharField(max_length=500, null=True, blank=True)

    datos_proceso = models.ForeignKey(DatosProceso, on_delete=models.CASCADE, related_name='sociedad', null=True, blank=True)

    url_drive = models.CharField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.nombre)

    @property
    def estado_badge(self):
        badge = {
            self.PENDIENTE: 'badge-warning',
            self.CERRADA: 'badge-primary',
            self.REVISADA: 'badge-primary',
            self.CONFIRMADA: 'badge-primary',
            self.ESTAMPADA: 'badge-primary',
            self.FINALIZADA: 'badge-success',
            self.CANCELADA: 'badge-danger',
        }
        return badge[self.estado]

    @property
    def apoderado(self):
        for socio in self.socio.all():
            if socio.apoderado == 'S':
                return '{}'.format(socio)


class Log(models.Model):
    sociedad = models.ForeignKey(Sociedad, on_delete=models.CASCADE, related_name='logs')
    fecha = models.DateTimeField(auto_now_add=True)
    usuario = models.ForeignKey(User, on_delete=models.PROTECT, related_name='logs_sociedad')
    descripcion = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Bitácora de Sociedades'

    def __str__(self):
        return self.descripcion


class Socio(models.Model):
    fecha_registro = models.DateTimeField(auto_now_add=True)
    nombres = models.CharField(max_length=250)
    apellidos = models.CharField(max_length=250)
    aporte = models.DecimalField("Porcentaje de aporte", max_digits=8, decimal_places=2)
    sociedad = models.ForeignKey(Sociedad, on_delete=models.CASCADE, related_name='socio')

    SINO = Choices(
        ("N", "No"),
        ("S", "Si"),
    )
    apoderado = models.CharField(max_length=1, choices=SINO, default="N")

    def __str__(self):
        return '{}, {}'.format(self.apellidos, self.nombres)
