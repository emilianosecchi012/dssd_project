from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from django.conf import settings
import os

class GoogleDriveAPIConector():

    def login(self):
        '''Realiza la autenticación frente a la API de Google Drive. Si se pudo realizar, retorna una instancia del objeto GoogleDrive, None caso contrario.'''
        try:
            gauth = GoogleAuth()
            gauth.LoadCredentialsFile(settings.ACCESS_TOKEN)
            if gauth.access_token_expired:
                gauth.Refresh()
                gauth.SaveCredentialsFile(settings.ACCESS_TOKEN)
            else:
                gauth.Authorize()
            return GoogleDrive(gauth)
        except Exception as excep:
            print("No se pudo realizar la conexión con la API de Google Drive.")
            print(excep)
            return None
    
    def subir_documento(self, nombre_archivo):
        '''Realiza la subida de un archivo a la API de Google Drive. Si se pudo realizar, retorna la URL del archivo almacenado, None caso contrario.'''
        try:
            drive = self.login()
            archivo = drive.CreateFile({'title': nombre_archivo})
            archivo.SetContentFile(settings.CARPETA_EXPEDIENTES + os.path.sep + nombre_archivo)
            archivo.Upload() 
            permission = archivo.InsertPermission({
                'type': 'anyone',
                'value': 'anyone',
                'role': 'reader'})
            print('title: %s, id: %s' % (archivo['title'], archivo['id']))
            print("-----------------")
            url_archivo_subido = archivo['alternateLink']
            print("URL del archivo subido: {}".format(url_archivo_subido))
            return url_archivo_subido
        except Exception as excep:
            print("No se pudo realizar la subida del archivo.")
            print(excep)
            return None