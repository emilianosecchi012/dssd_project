from django.urls import path
from .views import Descargar

app_name = 'core'

urlpatterns = [
    path('descargar/<str:archivo>', Descargar.as_view(), name='descargar'),
]
