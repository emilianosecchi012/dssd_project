from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, HTML, Field


class FormHelperMixin:
    def __init__(self, *args, **kwargs):
        super(FormHelperMixin, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'formulario'
        self.helper.layout = Layout()

        for key in self.fields.keys():
            self.helper.layout.fields.append(Field(key))

        self.helper.layout.fields.append(HTML(r"{% include 'includes/_botones_formulario_standar.html' %}"))

class FormHelperAjaxMixin:
    def __init__(self, *args, **kwargs):
        super(FormHelperAjaxMixin, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'formulario'
        self.helper.layout = Layout()

        for key in self.fields.keys():
            self.helper.layout.fields.append(Field(key))

        self.helper.layout.fields.append(HTML(r"{% include 'includes/_botones_formulario_ajax.html' %}"))


class ConfirmationMixin(object):
    """
    A mixin providing the ability to confirm objects
    """
    success_url = None

    def confirm(self, request, *args, **kwargs):
        """
        Redirects to the success URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    # Add support for browsers which only accept GET and POST for now.
    def post(self, request, *args, **kwargs):
        return self.confirm(request, *args, **kwargs)

    def get_success_url(self):
        if self.success_url:
            return self.success_url.format(**self.object.__dict__)
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
