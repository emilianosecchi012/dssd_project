from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View
from django.conf import settings
from django.http import HttpResponse, Http404
from django.views.generic.detail import (
    BaseDetailView, SingleObjectTemplateResponseMixin,
)
from magic import from_file
import os

from .mixins import ConfirmationMixin

class Descargar(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        file_path = os.path.join(settings.BASE_DIR, 'downloads', self.kwargs['archivo'])

        if os.path.exists(file_path):
            with open(file_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type=from_file(file_path, mime=True))
                response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
                return response
        raise Http404


class BaseConfirmView(ConfirmationMixin, BaseDetailView):
    """
    Base view for confirming an object.
    Using this base class requires subclassing to provide a response mixin.
    """


class ConfirmView(SingleObjectTemplateResponseMixin, BaseConfirmView):
    """
    View for confirming an object retrieved with `self.get_object()`,
    with a response rendered by template.
    """
    template_name_suffix = '_confirm'