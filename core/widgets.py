from bootstrap_datepicker_plus import DatePickerInput
from django.utils import timezone

DATEPICKER_TOOLTIPS = {
            'today': 'Hoy',
            'clear': 'Borrar',
            'close': 'Cerrar',
            'selectMonth': 'Seleccionar Mes',
            'prevMonth': 'Mes Previo',
            'nextMonth': 'Mes Proximo',
            'selectYear': 'Seleccionar Año',
            'prevYear': 'Año Previo',
            'nextYear': 'Año Proximo',
            'selectDecade': 'Seleccionar Decada',
            'prevDecade': 'Decada Previa',
            'nextDecade': 'Decada Proxima',
            'prevCentury': 'Siglos Previos',
            'nextCentury': 'Siglos Proximos ',
            'selectTime': 'Seleccionar Hora',
            'incrementHour': 'Incrementar Hora',
            'pickHour': 'Seleccionar Hora',
            'decrementHour': 'Decrementar Hora',
            'incrementMinute': 'Incrementar Minutos',
            'pickMinute': 'Seleccionar Minutos',
            'decrementMinute': 'Decrementar Minutos',
            'incrementSecond': 'Incrementar Segundos',
            'pickSecond': 'Seleccionar Segundos',
            'decrementSecond': 'Decrementar Segundos',
        }


class CalendarioWidget(DatePickerInput):
    opciones_defecto = {
        'locale': 'es',
        'format': 'DD/MM/YYYY',
        'tooltips': DATEPICKER_TOOLTIPS,
    }

    def __init__(self, options={}):
        self.opciones_defecto.update(options)
        super(CalendarioWidget, self).__init__(options=self.opciones_defecto)