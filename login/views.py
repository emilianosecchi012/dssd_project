from django.contrib.auth.views import LoginView

from .forms import LoginForm


class Login(LoginView):
    template_name = 'formulario_standar.html'
    form_class = LoginForm

    def get_context_data(self, **kwargs):
        context = super(Login, self).get_context_data(**kwargs)
        context['title'] = 'Login'
        return context
