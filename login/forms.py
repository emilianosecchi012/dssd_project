from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm

from core.forms import ModelFormBase

User = get_user_model()


class LoginForm(AuthenticationForm, ModelFormBase):
    class Meta:
        model = User
        fields = (
            'username',
            'password',
        )
