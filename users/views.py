from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LogoutView
from django.views.generic import TemplateView

User = get_user_model()

class Logout(LogoutView):
    if settings.DIRECCION_APP:
        next_page = 'https://{}.com/login'.format(settings.DIRECCION_APP)
    else:
        next_page = 'http://127.0.0.1:8000/login'

class Escritorio(LoginRequiredMixin, TemplateView):
    template_name = 'pages/home.html'