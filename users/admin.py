from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import User


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):
    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


ADDITIONAL_USER_FIELDS = (
    # (None, {'fields': ('some_additional_field',)}),
    ('Perfil', {'fields': ('empleado_mesa_entrada', 'empleado_area_legales', 'personal_direccion', 'usuario_bonita', 'password_bonita', 'usuario_estampillado', 'password_estampillado')}),
)


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    add_fieldsets = ADDITIONAL_USER_FIELDS + AuthUserAdmin.add_fieldsets
    fieldsets = ADDITIONAL_USER_FIELDS + AuthUserAdmin.fieldsets
    list_display = ('username', 'first_name', 'last_name', 'is_superuser', 'empleado_mesa_entrada', 'empleado_area_legales', 'personal_direccion')
    search_fields = ['username', 'last_name', ]
    list_filter = ('empleado_mesa_entrada', 'empleado_area_legales', 'personal_direccion', 'groups',)
