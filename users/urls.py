from django.urls import path

from .views import Logout, Escritorio

app_name = "users"

urlpatterns = [
    path('', Escritorio.as_view(), name='home'),
    path('logout/', Logout.as_view(), name='logout'),
]