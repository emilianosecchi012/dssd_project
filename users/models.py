from django.contrib.auth.models import AbstractUser
from django.db import models

class User(AbstractUser):
    empleado_mesa_entrada = models.BooleanField(default=False)
    empleado_area_legales = models.BooleanField(default=False)
    personal_direccion = models.BooleanField(default=False)
    usuario_bonita = models.CharField(max_length=50, null=True, blank=True)
    password_bonita = models.CharField(max_length=50, null=True, blank=True)
    usuario_estampillado = models.CharField(max_length=50, null=True, blank=True)
    password_estampillado = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.last_name, self.first_name)