# Generated by Django 3.1.12 on 2021-11-22 21:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20211025_1505'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='personal_direccion',
            field=models.BooleanField(default=False),
        ),
    ]
