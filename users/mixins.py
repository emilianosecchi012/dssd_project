from django.contrib.auth.mixins import UserPassesTestMixin


class EsEmpleadoMesaEntradaMixin(UserPassesTestMixin):

    def test_func(self):
        return self.request.user.empleado_mesa_entrada

class EsEmpleadoAreaLegalesMixin(UserPassesTestMixin):

    def test_func(self):
        return self.request.user.empleado_area_legales

class EsApoderadoMixin(UserPassesTestMixin):

    def test_func(self):
        return not self.request.user.empleado_mesa_entrada and not self.request.user.empleado_area_legales
