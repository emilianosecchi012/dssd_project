pytz==2020.4  # https://github.com/stub42/pytz
python-slugify==4.0.1  # https://github.com/un33k/python-slugify
Pillow  # https://github.com/python-pillow/Pillow
argon2-cffi==20.1.0  # https://github.com/hynek/argon2_cffi
whitenoise==5.2.0  # https://github.com/evansd/whitenoise
redis==3.5.3  # https://github.com/andymccurdy/redis-py
hiredis==1.1.0  # https://github.com/redis/hiredis-py
pyopenssl
python-magic
requests
asgiref==3.4.1
cachetools==4.2.4
certifi==2021.5.30
cffi==1.14.6
charset-normalizer==2.0.6
cryptography==35.0.0
google-api-core==2.2.2
google-api-python-client==2.31.0
google-auth==2.3.3
google-auth-httplib2==0.1.0
google-auth-oauthlib==0.4.6
googleapis-common-protos==1.53.0
httplib2==0.20.2
idna==3.2
oauth2client==4.1.3
oauthlib==3.1.1
protobuf==3.19.1
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycparser==2.20
PyDrive==1.3.1
pyOpenSSL==21.0.0
pyparsing==3.0.6
PyYAML==6.0
reportlab==3.6.2
requests-oauthlib==1.3.0
rsa==4.7.2
six==1.16.0
sqlparse==0.4.2
text-unidecode==1.3
uritemplate==4.1.1
urllib3==1.26.7

# Django
# ------------------------------------------------------------------------------
django==3.1.12  # pyup: < 3.1  # https://www.djangoproject.com/
django-environ==0.4.5  # https://github.com/joke2k/django-environ
django-model-utils==4.0.0  # https://github.com/jazzband/django-model-utils
django-crispy-forms==1.9.2  # https://github.com/django-crispy-forms/django-crispy-forms
django-extensions==3.0.9
django-bootstrap-datepicker-plus