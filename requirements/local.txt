-r base.txt

Werkzeug==1.0.1 # https://github.com/pallets/werkzeug
argon2-cffi==20.1.0  # https://github.com/hynek/argon2_cffi

# Django
# ------------------------------------------------------------------------------
django-debug-toolbar==3.1.1  # https://github.com/jazzband/django-debug-toolbar
django-extensions==3.0.9  # https://github.com/django-extensions/django-extensions
