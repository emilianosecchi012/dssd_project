from reportlab.pdfgen import canvas 
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from django.conf import settings
import os

def drawMyRuler(pdf):
    pdf.drawString(100,810, 'x100')
    pdf.drawString(200,810, 'x200')
    pdf.drawString(300,810, 'x300')
    pdf.drawString(400,810, 'x400')
    pdf.drawString(500,810, 'x500')

    pdf.drawString(10,100, 'y100')
    pdf.drawString(10,200, 'y200')
    pdf.drawString(10,300, 'y300')
    pdf.drawString(10,400, 'y400')
    pdf.drawString(10,500, 'y500')
    pdf.drawString(10,600, 'y600')
    pdf.drawString(10,700, 'y700')
    pdf.drawString(10,800, 'y800')

def generar_pdf(sociedad, fecha_aprobacion, nombre_archivo):
    print("Comienza la generación del expediente digital de la sociedad.")

    documentTitle = 'Expediente digital - Sociedad'
    title = 'Expediente digital'
    subTitle = 'Sociedad anonima aprobada'

    texto_mensaje = [
        'Su sociedad ha sido aprobada el día {} por el personal de legales.'.format(fecha_aprobacion),
        'La documentación ya se encuentra disponible para ser retirada personalmente o',
        'descargada por este medio.'
    ]

    atributos_sociedad = [
        'Nombre de la sociedad: {}'.format(sociedad.nombre),
        'Fecha de creación: {}'.format(sociedad.fecha_creacion),
        'Domicilio legal: {}'.format(sociedad.domicilio_legal),
        'Domicilio real: {}'.format(sociedad.domicilio_real),
        'Apoderado: {}'.format(sociedad.apoderado),
        'Código único de identificación nacional: {}'.format(sociedad.hash),
        'Para más información, escanee el código QR adjunto en el archivo.'
    ]

    pdf = canvas.Canvas(settings.CARPETA_EXPEDIENTES + os.path.sep + nombre_archivo)
    pdf.setTitle(documentTitle)

    ## Guías
    ## drawMyRuler(pdf)

    # Registrar font nueva.
    path_font = settings.STATICFILES_DIRS[0] + os.path.sep + "fonts" + os.path.sep + "Louis George Cafe.ttf"
    pdfmetrics.registerFont(
        TTFont('luisGeorgeCafeFont', path_font)
    )

    pdf.setFont('luisGeorgeCafeFont', 32)
    pdf.drawCentredString(300, 770, title)

    ## Sub titulo
    pdf.setFont("luisGeorgeCafeFont", 18)
    pdf.drawCentredString(290,720, subTitle)

    ## Separador
    pdf.line(100, 710, 500, 710)

    pdf.setFont("luisGeorgeCafeFont", 14)
    text = pdf.beginText(40, 680)
    for line in texto_mensaje:
        text.textLine(line)
    pdf.drawText(text)

    pdf.setFont("luisGeorgeCafeFont", 18)
    pdf.drawCentredString(290,600, "Información de la sociedad")
    
    pdf.setFont("luisGeorgeCafeFont", 14)
    text = pdf.beginText(40, 570)
    for line in atributos_sociedad:
        text.textLine(line)
    pdf.drawText(text)

    ## Imagen QR.
    sub_string = '..' + os.path.sep + '..' + os.path.sep + "media" + os.path.sep
    path_qr = sociedad.imagen_qr.replace(sub_string, '')
    print(path_qr)
    full_path_qr = settings.MEDIA_ROOT + os.path.sep + path_qr
    pdf.drawImage(full_path_qr, 225, 280, width=150, height=150, mask=None, preserveAspectRatio=True, anchor='n')
    
    pdf.save()